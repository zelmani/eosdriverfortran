# README #

* Fortran (mostly Fortran 90) version of the equation of state driver routines for O'Connor & Ott and Schneider, Roberts, & Ott nuclear equation of state (EOS) tables.
* Version 1.0 (master branch is development branch)